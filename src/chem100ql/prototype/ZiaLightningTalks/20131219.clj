(ns chem100ql.prototype.ZiaLightningTalks.20131219
  (:require [chem100.ql :as ql]))
(use '[clojure.string :only (join split)])

(ql/connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")

(ql/exec-select "select dpp:personId from dpp:person where dpp:personId like 'mst%'")
(ql/exec-delete "delete from dpp:person where dpp:personId like 'mstang%'")

(ql/exec-select "select cmis:path from dpp:article where dpp:manuscriptNumber = 1236503")

(ql/exec-insert "insert folder into path '/' (cmis:name, cmis:objectTypeId) values ('A Testing Folder','cmis:folder')")
(ql/exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('ATestFolder2', 'F:dpp:collection' , 'productApproved')")
(ql/exec-insert "insert folder into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:collectionType) values ('Test', 'F:dpp:collection', 'productUnapproved')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId) values ('Hello World', 'cmis:document')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson', 'D:dpp:person', 'mstang@ziaconsulting.com')")


(ql/exec-select "select dpp:middleName,dpp:firstName from dpp:person where dpp:firstName = 'Mark'")
(ql/exec-update "update dpp:person set dpp:middleName = '20131219', dpp:firstName = 'Mark' where dpp:firstName = 'Mark'")
(ql/exec-select "select dpp:middleName,dpp:firstName from dpp:person where dpp:firstName = 'Mark'")

(ql/exec-select "select cmis:name from cmis:document where cmis:name like 'Test%'")

(ql/exec-delete "delete from cmis:folder where cmis:name = '/A Testing Folder'")
(ql/exec-delete "delete from cmis:document where cmis:name = 'Test1'")
(ql/exec-delete "delete from cmis:document where cmis:name = 'Test2'")
